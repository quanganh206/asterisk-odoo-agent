import logging
import os
import requests
import ssl
from urllib.parse import urljoin
import click
import tornado.web
from tornado.ioloop import IOLoop
from terminado import TermSocket, UniqueTermManager
from tornado.web import HTTPError
from tornado.httpserver import HTTPServer

logging.basicConfig(
    level=logging.DEBUG if os.getenv('DEBUG') else logging.INFO,
    format='%(asctime)s - %(name)s:%(lineno)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)


class MyTermSocket(TermSocket):

    def check_origin(self, origin):
        return True

    def get(self, *args, **kwargs):
        server_id = self.get_argument('server')
        db = self.get_argument('db')
        if not server_id:
            raise HTTPError(404)
        # Check auth token
        auth = self.get_argument('auth')
        res = requests.get(urljoin(
            os.environ['ODOO_URL'], '/asterisk_base/console/{}/{}/{}'.format(
                db, server_id, auth)))
        res.raise_for_status()
        if res.text != 'ok':
            raise HTTPError(403)
        return super(TermSocket, self).get(*args, **kwargs)


@click.command()
@click.option('--listen-address', default='127.0.0.1',
              envvar='LISTEN_ADDRESS', help='Listen address.')
@click.option('--listen-port', default=8001, type=int,
              envvar='LISTEN_PORT', help='Listen port.')
@click.option('--odoo-url', default='http://127.0.0.1:8069',
              envvar='ODOO_URL', help='Odoo URL.')
@click.option('--enable-ssl', is_flag=True, 
              envvar='SSL_ENABLED', help='Enable SSL.')
def main(listen_address, listen_port, odoo_url, enable_ssl):
    logger.info('Starting Asterisk CLI server at %s:%s.',
                listen_address, listen_port)
    logger.info('Odoo configured at %s.', odoo_url)
    # Set ODOO_URL envvar
    os.environ['ODOO_URL'] = odoo_url
    term_manager = UniqueTermManager(
        shell_command=['asterisk', '-rcvvv'])
    handlers = [
        (r'/', MyTermSocket, {'term_manager': term_manager})]
    # Create SSL context
    if enable_ssl:
        # ssl_ctx = ssl.create_default_context()
        ssl_ctx = ssl.SSLContext()
        ssl_ctx.check_hostname = False
        # ssl_ctx.verify_mode = ssl.CERT_NONE
    else:
        ssl_ctx = None
    # Change working directory to Amanita root dir
    app = tornado.web.Application(handlers)
    server = HTTPServer(app, ssl_options=ssl_ctx)    
    server.listen(listen_port, address=listen_address)
    IOLoop.current().start()


if __name__ == '__main__':
    main()
