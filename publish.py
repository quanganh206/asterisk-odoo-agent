#!/usr/bin/python3
import click
import os
import re
import subprocess

# Default value for RABBITMQ URI.
RABBITMQ = '127.0.0.1:5672'


def get_version():
    version_file = open(
        os.path.join(
            os.path.dirname(__file__), 'asterisk_odoo_agent', '__init__.py')
    ).read()
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


@click.command()
@click.option('--version')
@click.option('--push', is_flag=True)
@click.option('--latest', is_flag=True)
@click.option('--git-push', is_flag=True)
@click.option('--publish', is_flag=True)
@click.option('--no-cache', is_flag=True)
def build(version, push, no_cache, publish, git_push, latest):
    if not version:
        version = get_version()
    if git_push:
        os.system('git push')
    if publish:
        os.system('python3 setup.py sdist')
        os.system(
            'twine upload dist/asterisk-odoo-agent-{}.tar.gz'.format(version))
    subprocess.check_call(
        'docker buildx build {} --pull --build-arg AGENT_VERSION={} \
        --platform linux/amd64,linux/arm/v7 \
        --build-arg RABBITMQ={} -t odooist/asterisk-odoo-agent:{} {} .'.format(
        '' if not no_cache else '--no-cache', version, RABBITMQ, version,
        '' if not push else '--push'),
        shell=True)
    if latest:
        subprocess.check_call(
            'docker buildx build {} --pull --build-arg AGENT_VERSION={} \
            --platform linux/amd64,linux/arm/v7 \
            --build-arg RABBITMQ={} -t odooist/asterisk-odoo-agent:latest {} .'.format(
            '' if not no_cache else '--no-cache', version, RABBITMQ,
            '' if not push else '--push'),
            shell=True)


if __name__ == '__main__':
    build()
