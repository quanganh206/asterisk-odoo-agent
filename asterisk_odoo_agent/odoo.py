import eventlet
import json
import logging
# Used from event map
import random
from nameko.events import event_handler
from nameko.dependency_providers import Config
from nameko.rpc import RpcProxy, rpc
from nameko_odoo.bus_poller import bus
from nameko_odoo.http_entry import http
from nameko_odoo.rpc_client import OdooClient
import os
from .dependencies.events_map import EventsMap
from .dependencies.start import on_start

logger = logging.getLogger(__name__)

# Set here asterisk.conf: systemname.
SYSTEM_NAME = os.getenv('SYSTEM_NAME', 'asterisk')


class OdooService:
    name = 'odoo'
    config = Config()
    odoo = OdooClient()
    # Proxy to AMI broker.
    asterisk = RpcProxy('{}_ami'.format(SYSTEM_NAME))
    events_map = EventsMap()

    @on_start
    def on_service_start(self):
        # Load events
        try:
            if not self.odoo_connected.ready():
                logger.info('Waiting for Odoo connection to load events...')
                self.odoo_connected.wait()
                logger.info('Odoo connected. Loading events...')
            self.reload_events()
        except Exception as e:
            logger.error('Error loading events: %s', e)
        # Check for on start procedures.
        if self.config.get('ODOO_ON_START_EXECUTE'):
            # Wait a bit to complete initialization.
            eventlet.sleep(int(self.config.get('ODOO_ON_START_DELAY', 5)))
            logger.debug('Odoo service on start.')
            executes = self.config.get('ODOO_ON_START_EXECUTE')
            if type(executes) is str:
                executes = [executes]
            try:
                for execute in executes:
                    app, model, method = execute.split('.')
                    logger.debug('Calling %s.%s.%s.', app, model, method)
                    self.execute('{}.{}'.format(app, model), method)
            except Exception:
                logger.exception('Execute on start error')

    def reload_events(self):
        ev_map = []
        for ev_id in self.odoo.env['asterisk_common.event'].search(
                [('is_enabled', '=', True)]):
            ev = self.odoo.env['asterisk_common.event'].browse(ev_id)
            ev_map.append({
                'source': ev.source,
                'name': ev.name,
                'model': ev.model,
                'method': ev.method,
                'condition': ev.condition,
                'delay': ev.delay,
            })
            logger.info('Loaded %s %s event for %s:%s',
                        ev.name, ev.source, ev.model, ev.method)
        self.events_map.events_map = ev_map

    @rpc
    def notify_user(self, uid, message, title='Notification',
                    level='info', sticky=False):
        # Helper func used from services to sent Odoo user notifications.
        if not uid:
            logger.debug('No uid, will not notify')
            return
        logger.debug('Notify user %s: %s', uid, message)
        self.odoo.env['bus.bus'].sendone('notify_{}_{}'.format(level, uid), {
            'message': message,
            'sticky': sticky,
            'title': title
        })

    @bus
    def on_bus_message(self, channel, message):
        logger.debug('On bus message: %s', message)
        return self.on_command(channel, message)

    @http('POST', '/')
    def on_http_message(self, request):
        try:
            message = json.loads(request.get_data(as_text=True))
        except Exception as e:
            logger.exception('HTTP message parse error:')
            return {'error': str(e)}
        logger.debug('On bus message: %s', message)
        # Be aware that HTTP respone will be sent only after on_command return.
        return self.on_command('http', message)

    def on_command(self, channel, message):
        try:
            command = 'on_command_{}'.format(message.get('command'))
            if hasattr(self, command):
                logger.debug('Calling %s', command)
                return getattr(self, command)(channel, message)
            else:
                logger.error('No command for message: %s', message)
        except Exception:
            logger.exception('Bus message error:')

    @event_handler(SYSTEM_NAME + '_ami', '*')
    def on_ami_event(self, event):
        # Trace AMI events if ODOO_TRACE_AMI_EVENTS=yes in config.yml.
        if self.config.get('ODOO_TRACE_AMI_EVENTS'):
            logger.info('AMI event: %s', json.dumps(event, indent=2))
        try:
            event_name = event.get('Event')
            handlers = [k for k in self.events_map.events_map if k[
                'name'] == event_name and k['source'] == 'AMI']
            if not handlers:
                # We did not find any event handlers.
                logger.debug('Ignoring AMI event %s', event_name)
                return
            # Take the handlers and call them one by one
            for handler in handlers:
                if handler.get('condition'):
                    # Handler has a condition so evaluate it first.
                    try:
                        res = eval(handler['condition'],
                                   None, {'event': event})
                        if not res:
                            # The confition evaluated to False so do not send.
                            logger.debug(
                                'Event %s condition evaluated to False.',
                                event_name)
                            continue
                    except Exception:
                        logger.exception(
                            'Error evaluating condition: %s, event: %s',
                            handler['condition'], event)
                        # The confition evaluated to error so do not send.
                        continue
                # Sometimes it's required to send event to Odoo with a delay.
                delay = float(eval(str(handler.get('delay', '0'))))
                if delay:
                    logger.debug('%s.%s delay %s',
                                 handler['model'], handler['method'], delay)
                    eventlet.spawn_after(
                        delay, self.send_event, handler['model'],
                        handler['method'], event)
                else:
                    # When no delay is set we do not spawn so that AMI events
                    # can be received by Odoo as they appear.
                    self.send_event(handler['model'], handler['method'], event)
        except Exception:
            logger.exception('[AMI_EVENT_ERROR]')

    def send_event(self, model, method, event):
        logger.debug('Sending AMI event to Odoo, %s.%s(%s)',
                     model, method, event)
        try:
            self.odoo.execute(model, method, event)
        except Exception as e:
            logger.error('[ODOO_RPC_ERROR] %s, %s', e, event)

    @rpc
    def execute(self, model, method, *args, **kwargs):
        return self.odoo.execute(model, method, *args, **kwargs)

    def on_command_reload_events(self, channel={}, message={}):
        logger.info('Reloading events...')
        self.reload_events()
        if message.get('notify_uid'):
            self.odoo.env['remote_agent.agent'].bus_sendone(
                'remote_agent_notification_{}'.format(message['notify_uid']),
                {'message': 'Events reloaded.', 'title': 'Asterisk Agent'})
