import eventlet
from eventlet.greenio import GreenPipe
import logging
from nameko.extensions import Entrypoint
import os
import stat

logger = logging.getLogger(__name__)


class QueueLogReader(Entrypoint):
    path = None
    pipe = None
    setup_failed = False    

    def setup(self):
        if not self.container.config.get('QUEUE_LOG_ENABLED'):
            logger.info('Queue log service is not enabled.')
            return
        self.path = self.container.config.get('QUEUE_LOG_PATH',
                                              '/var/log/asterisk/queue_log')
        if not os.path.exists(self.path):
            logger.error('Pipe %s does not exist. Create it first.', self.path)
            self.setup_failed = True
        elif not stat.S_ISFIFO(os.stat(self.path).st_mode):
            logger.error(
                'File %s is not a pipe. Pls use mkfifo command to create it '
                'with proper permissions', self.path)
            self.setup_failed = True

    def start(self):
        if not self.container.config.get('QUEUE_LOG_ENABLED'):
            return
        if not self.setup_failed:
            self.container.spawn_managed_thread(self.run)

    def stop(self):
        if self.pipe:
            self.pipe.close()
            logger.debug('Pipe %s closed.', self.path)

    def run(self):
        try:
            logger.info('Opening pipe %s', self.path)
            fd = os.open(self.path, os.O_RDONLY | os.O_NONBLOCK)
            self.pipe = GreenPipe(fd, 'r')
            while True:
                line = self.pipe.readline()[:-1]
                if line:
                    logger.debug('pipe received: %s', line)
                    self.container.spawn_worker(self, (line,), {})
                eventlet.sleep(.1)
        except ValueError as e:
            if 'readline of closed file' in str(e):
                return
        except Exception:
            logger.exception('Error')


queue_log = QueueLogReader.decorator
