#!/bin/bash

RABBITMQ=${RABBITMQ:-rabbitmq}
RABBITMQ_TIMEOUT=${RABBITMQ_TIMEOUT:-60}

for ((i=0;i<=$RABBITMQ_TIMEOUT;i++)); do
    amqp-declare-queue -u amqp://${RABBITMQ} -q test > /dev/null
    if [ $? -eq 0 ]
    then
        break
    else
        if [ $i -eq $RABBITMQ_TIMEOUT ]
        then
            printf "Failed to find a working RabbitMQ service. Exiting"
            exit 1
        fi
    fi
    sleep 1
done

exec nameko run --config=config.yml asterisk_odoo_agent
